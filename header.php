<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="description" content="<?php echo esc_attr(get_bloginfo('description')); ?>" />
    <title><?php wp_title('-', true, 'right'); ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php get_template_part('partials/favicons'); ?>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <?php get_template_part('partials/barrabrasil'); ?>
    <section class="container">
        <div class="row">
    		<div class="col-xs-12">
    			<p class="pull-right" id="header-links"><a href="<?php bloginfo('url'); ?>">P&aacute;gina Inicial</a> | <a href="//www.ifrs.edu.br/">Portal do IFRS</a></p>
    		</div>
    	</div>
    </section>
    <a href="#inicio-conteudo" id="inicio-conteudo" class="sr-only">In&iacute;cio do conte&uacute;do</a>
