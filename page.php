<?php get_header(); ?>

<?php the_post(); ?>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <div class="row">
                <header class="col-xs-12">
                    <?php get_template_part('partials/logo-home-link'); ?>
                </header>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <a href="<?php bloginfo('url'); ?>" title="Voltar para a P&aacute;gina Inicial" id="link-home"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;&nbsp;Voltar para a P&aacute;gina Inicial</a>
                </div>
            </div>
            <?php get_template_part('partials/social-media'); ?>
        </div>
        <div class="col-xs-12 col-md-8" id="content">
            <?php if (has_post_thumbnail()) : ?>
                <div class="row">
                    <div class="col-xs-12" id="post-image">
                        <?php the_post_thumbnail('post-thumbnail', array('class' => 'img-responsive')); ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row" id="post-meta">
                <div class="col-xs-12" id="post-social">
                    Compartilhar:&nbsp;
                    <a href="http://twitter.com/intent/tweet?status=<?php echo get_the_title(); ?>+<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/share-twitter.png" alt="Compartilhar no Facebook"/></a>
                    &nbsp;
                    <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>&amp;title=<?php echo get_the_title(); ?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/share-facebook.png" alt="Compartilhar no Facebook"/></a>
                </div>
            </div>
            <div class="row" id="post-title">
                <div class="col-xs-12">
                    <h2><?php the_title(); ?></h2>
                </div>
            </div>
            <div class="row" id="post-content">
                <div class="col-xs-12">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
