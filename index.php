<?php get_header(); ?>

<div class="container">
	<div class="row" id="ms-grid">
		<header class="col-xs-12 col-md-4 ms-item">
			<?php get_template_part('partials/logo-home-link'); ?>
		</header>
		<div class="col-xs-12 col-md-4 ms-item">
			<div class="row">
				<div class="col-xs-12">
					<div id="home-sobre">
						<p>A campanha Mundo Melhor foi criada para provocar reflex&otilde;es sobre os nossos atos e h&aacute;bitos e no que ele implicam no presente e futuro.</p>
						<p>Apresentamos dicas e curiosidades, boas pr&aacute;ticas e projetos desenvolvidos no IFRS e muito mais.</p>
					</div>
				</div>
			</div>
			<?php get_template_part('partials/social-media'); ?>
		</div>

		<?php
		    global $wp_query;
		    $args = array(
		        'tax_query' => array(
		            array(
		                'taxonomy' => 'post_format',
		                'field'    => 'slug',
		                'terms'    => array(
							'post-format-aside',
			                'post-format-audio',
			                'post-format-chat',
			                'post-format-gallery',
			                'post-format-image',
			                'post-format-link',
			                'post-format-quote',
			                'post-format-status',
			                'post-format-video',
						),
						'operator' => 'NOT IN',
		            ),
		        ),
		    );
		    $args = array_merge($wp_query->query_vars, $args);
		    $posts = new WP_Query($args);
		?>

		<?php if ($posts->have_posts()) : ?>
			<?php while ($posts->have_posts()) : $posts->the_post(); ?>
				<div class="col-xs-12 col-md-4 ms-item">
					<div <?php post_class(); ?>>
						<?php if (has_post_thumbnail()) { the_post_thumbnail('post-thumbnail', array('class' => 'img-responsive center-block')); } ?>
						<h2><?php the_title(); ?></h2>
						<a class="btn btn-default btn-xs pull-right" href="<?php the_permalink(); ?>"><span class="glyphicon glyphicon-hand-right"></span> Leia Mais</a>
						<div class="clearfix"></div>
					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>

		<?php wp_reset_postdata(); ?>

		<?php
		    global $wp_query;
		    $args = array(
		        'tax_query' => array(
		            array(
		                'taxonomy' => 'post_format',
		                'field'    => 'slug',
		                'terms'    => array('post-format-status', 'post-format-aside', 'post-format-video', 'post-format-image'),
		            ),
		        ),
		    );
		    $args = array_merge($wp_query->query_vars, $args);
		    $formats = new WP_Query($args);
		?>

		<?php if ($formats->have_posts()) : ?>
			<?php while ($formats->have_posts()) : $formats->the_post(); ?>
				<div class="col-xs-12 col-md-4 ms-item">
				<?php if (get_post_format() === 'status') : ?>
					<div <?php post_class(); ?>>
						<div class="row">
							<div class="col-xs-5">
								<?php if (has_post_thumbnail()) { the_post_thumbnail('post-thumbnail', array('class' => 'img-responsive center-block')); } ?>
							</div>
							<div class="col-xs-7 valign">
								<h2><?php the_title(); ?></h2>
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				<?php elseif (get_post_format() === 'aside') : ?>
					<div <?php post_class(); ?>>
						<div class="row">
							<div class="col-xs-12">
								<?php if (has_post_thumbnail()) { the_post_thumbnail('post-thumbnail', array('class' => 'img-responsive center-block')); } ?>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<h2><?php the_title(); ?></h2>
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				<?php elseif (get_post_format() === 'video') : ?>
					<div <?php post_class(); ?>>
						<div class="row">
							<div class="col-xs-12 col-md-7">
								<div class="embed-responsive embed-responsive-4by3">
									<?php the_content(); ?>
								</div>
							</div>
							<div class="col-xs-12 col-md-5">
								<h2><small>V&iacute;deo</small><br/><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							</div>
						</div>
					</div>
				<?php elseif (get_post_format() === 'image') : ?>
					<div <?php post_class(); ?>>
						<div class="row">
							<div class="col-xs-12">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>
