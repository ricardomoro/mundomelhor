module.exports = function(grunt) {
grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    clean: {
        dist: {
            src: ['dist'],
        },
    },

    copy: {
        dist: {
            expand: true,
            cwd: '.',
            src: ['**', '!.**', '!less/**', '!node_modules/**', '!bower.json', '!Gruntfile.js', '!package.json'],
            dest: 'dist/',
        },
    },

    cssmin: {
        target: {
            files: [{
                expand: true,
                cwd: 'css',
                src: ['*.css', '!*.min.css'],
                dest: 'css',
                ext: '.min.css',
            }],
        },
    },

    favicons: {
        options: {
            html: 'partials/favicons.php',
            HTMLPrefix: "<?php echo get_stylesheet_directory_uri(); ?>/favicons/",
            tileBlackWhite: false,
        },
        icons: {
            src: 'img/favicon.source.png',
            dest: 'favicons',
        },
    },

    imagemin: {
        dynamic: {
            files: [{
                expand: true,
                cwd: 'img/',
                src: ['*.{png,jpg,gif}'],
                dest: 'img/',
            }],
        },
    },

    less: {
        dist: {
            options: {
                paths: ["less"],
            },
            files: {
                "css/mundomelhor.css": "less/mundomelhor.less",
            },
        },
    },

    watch: {
        options: {
            livereload: true,
        },
        php: {
            files: '**/*.php',
            tasks: [],
        },
        favicon: {
            files: 'img/favicon.source.png',
            tasks: ['favicons'],
        },
        less: {
            files: 'less/*.less',
            tasks: ['less:dist'],
        },
    },
});

    // Load plugins

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-favicons');


    // Default task(s).
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('build', [
        'clean',
        'imagemin',
        'favicons',
        'less',
        'cssmin']);
    grunt.registerTask('dist', [
        'build',
        'copy'
    ]);
};
