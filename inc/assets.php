<?php
function load_styles() {
    // wp_enqueue_style( $handle, $src, $deps, $ver, $media );
    if (WP_DEBUG) {
        wp_enqueue_style( 'style', get_template_directory_uri().'/css/mundomelhor.css', array(), false, 'all' );
    } else {
        wp_enqueue_style( 'style', get_template_directory_uri().'/css/mundomelhor.min.css', array(), false, 'all' );
    }
}

function load_scripts() {
    if ( ! is_admin() ) {
        wp_deregister_script('jquery');
    }

    // wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );
    if (WP_DEBUG) {
        wp_enqueue_script( 'html5shiv', get_template_directory_uri().'/vendor/html5shiv/dist/html5shiv.js', array(), false, false );
        wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );

        wp_enqueue_script( 'respond', get_template_directory_uri().'/vendor/respond-minmax/dest/respond.src.js', array(), false, false );
        wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

        wp_enqueue_script( 'jquery-custom', get_template_directory_uri().'/vendor/jquery/dist/jquery.js', array(), false, false );
        wp_enqueue_script( 'bootstrap', get_template_directory_uri().'/vendor/bootstrap/dist/js/bootstrap.js', array(), false, false );

        wp_enqueue_script( 'masonry-custom', get_template_directory_uri().'/vendor/masonry/dist/masonry.pkgd.js', array(), false, true );
        wp_enqueue_script( 'masonry-config', get_template_directory_uri().'/js/masonry.config.js', array('masonry-custom'), false, true );

        wp_enqueue_script( 'valign', get_template_directory_uri().'/vendor/jQuery-Flex-Vertical-Center/jquery.flexverticalcenter.js', array(), false, false );
        wp_enqueue_script( 'valign-config', get_template_directory_uri().'/js/valign.config.js', array(), false, true );
    } else {
        wp_enqueue_script( 'html5shiv', get_template_directory_uri().'/vendor/html5shiv/dist/html5shiv.min.js', array(), false, false );
        wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );

        wp_enqueue_script( 'respond', get_template_directory_uri().'/vendor/respond-minmax/dest/respond.min.js', array(), false, false );
        wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

        wp_enqueue_script( 'jquery-custom', get_template_directory_uri().'/vendor/jquery/dist/jquery.min.js', array(), false, false );
        wp_enqueue_script( 'bootstrap', get_template_directory_uri().'/vendor/bootstrap/dist/js/bootstrap.min.js', array(), false, false );

        wp_enqueue_script( 'masonry-custom', get_template_directory_uri().'/vendor/masonry/dist/masonry.pkgd.min.js', array(), false, true );
        wp_enqueue_script( 'masonry-config', get_template_directory_uri().'/js/masonry.config.js', array('masonry-custom'), false, true );

        wp_enqueue_script( 'valign', get_template_directory_uri().'/vendor/jQuery-Flex-Vertical-Center/jquery.flexverticalcenter.js', array(), false, false );
        wp_enqueue_script( 'valign-config', get_template_directory_uri().'/js/valign.config.js', array(), false, true );

        wp_enqueue_script( 'js-barra-brasil', 'http://barra.brasil.gov.br/barra.js?cor=verde', array(), false, true );
    }
}

add_action( 'wp_enqueue_scripts', 'load_styles' );
add_action( 'wp_enqueue_scripts', 'load_scripts' );
