<?php
// Register Custom Taxonomy
function cores_taxonomy() {
	$labels = array(
		'name'                       => _x( 'Cores', 'Taxonomy General Name', 'mundomelhor' ),
		'singular_name'              => _x( 'Cor', 'Taxonomy Singular Name', 'mundomelhor' ),
		'menu_name'                  => __( 'Cores', 'mundomelhor' ),
		'all_items'                  => __( 'Todas as Cores', 'mundomelhor' ),
		'parent_item'                => __( 'Cor Mãe', 'mundomelhor' ),
		'parent_item_colon'          => __( 'Cor Mãe:', 'mundomelhor' ),
		'new_item_name'              => __( 'Nova Cor', 'mundomelhor' ),
		'add_new_item'               => __( 'Adicionar Nova Cor', 'mundomelhor' ),
		'edit_item'                  => __( 'Editar Cor', 'mundomelhor' ),
		'update_item'                => __( 'Atualizar Cor', 'mundomelhor' ),
		'view_item'                  => __( 'Ver Cor', 'mundomelhor' ),
		'separate_items_with_commas' => __( 'Separar Cores com vírgulas', 'mundomelhor' ),
		'add_or_remove_items'        => __( 'Adicionar ou Remover Cores', 'mundomelhor' ),
		'choose_from_most_used'      => __( 'Escolher pela cor mais usada', 'mundomelhor' ),
		'popular_items'              => __( 'Cores Populares', 'mundomelhor' ),
		'search_items'               => __( 'Buscar Cores', 'mundomelhor' ),
		'not_found'                  => __( 'Não encontrada', 'mundomelhor' ),
		'no_terms'                   => __( 'Sem Cores', 'mundomelhor' ),
		'items_list'                 => __( 'Lista de Cores', 'mundomelhor' ),
		'items_list_navigation'      => __( 'Lista de Navegação de Cores', 'mundomelhor' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => false,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'cor', array( 'post' ), $args );

}

add_action( 'init', 'cores_taxonomy', 0 );
