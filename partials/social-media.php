<div class="row" id="social-media">
    <div class="col-xs-3">
        <a href="//www.facebook.com/IFRSOficial" title="Página do IFRS no Facebook"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-facebook.png" alt="Página do IFRS no Facebook" class="img-responsive" /></a>
    </div>
    <div class="col-xs-3">
        <a href="//www.twitter.com/IF_RS" title="Twitter do IFRS"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-twitter.png" alt="Twitter do IFRS" class="img-responsive" /></a>
    </div>
    <div class="col-xs-3">
        <a href="//www.youtube.com/ComunicaIFRS" title="Canal do IFRS no YouTube"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-youtube.png" alt="Canal do IFRS no YouTube" class="img-responsive" /></a>
    </div>
    <div class="col-xs-3">
        <a href="//www.instagram.com/IFRSOficial" title="Perfil do IFRS no Instagram"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-instagram.png" alt="Perfil do IFRS no Instagram" class="img-responsive" /></a>
    </div>
</div>
