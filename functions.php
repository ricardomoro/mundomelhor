<?php
//Registra o menu principal
register_nav_menus(
    array(
        'menu-superior' => 'Menu Superior (Principal)',
    )
);

// Habilita a imagem destacada nos posts.
add_theme_support( 'post-thumbnails' );

// Habilita Post Formats
add_theme_support( 'post-formats', array( 'aside', 'status', 'video', 'image' ) );

// Menu do Bootstrap
require_once('inc/wp_bootstrap_navwalker.php');

// Script Conditional
require_once('inc/script_conditional.php');

// Scripts & Styles
require_once('inc/assets.php');

// Título na Home
require_once('inc/home_title.php');

// Taxonomy Cores
require_once('inc/taxonomy-cores.php');
