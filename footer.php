<a href="#fim-conteudo" id="fim-conteudo" class="sr-only">Fim do conte&uacute;do</a>

<footer class="container">
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <a href="//www.ifrs.edu.br/" title="Site do IFRS">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-ifrs-branco.png" alt="Logo do IFRS" class="logo"/>
            </a>
        </div>
        <div class="col-xs-12 col-md-4">
            <address>
                <p><strong>Mundo Melhor IFRS</strong></p>
                <p>Instituto Federal de Educa&ccedil;&atilde;o, Ci&ecirc;ncia e Tecnologia do Rio Grande do Sul</p>
                <p><a href="mailto:comunicacao@ifrs.edu.br">comunicacao@ifrs.edu.br</a></p>
            </address>
        </div>
        <div class="col-xs-12 col-md-4">
            <div id="footer-info-links">
                <p>
                    <!-- Wordpress -->
                    <a href="http://www.wordpress.org/" target="blank">Desenvolvido com Wordpress<span class="sr-only"> (abre uma nova p&aacute;gina)</span></a>&nbsp;<span class="glyphicon glyphicon-new-window"></span>
                </p>
                <p>
                    <!-- Código-fonte GPL -->
                    <a href="https://bitbucket.org/ricardomoro/mundomelhor/" target="blank">C&oacute;digo-fonte deste tema sob a licen&ccedil;a GPLv3<span class="sr-only"> (abre uma nova p&aacute;gina)</span></a>&nbsp;<span class="glyphicon glyphicon-new-window"></span>
                </p>
                <p>
                    <!-- Creative Commons -->
                    <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/cc-by-nc-nd.png" alt="M&iacute;dia licenciada sob a Licen&ccedil;a Creative Commons Atribui&ccedil;&atilde;o-N&atilde;oComercial-SemDeriva&ccedil;&otilde;es 4.0 Internacional (abre uma nova p&aacute;gina)" /></a>&nbsp;<span class="glyphicon glyphicon-new-window"></span>
                </p>
            </div>
        </div>
    </div>
</footer>
<!-- Javascript -->
<?php wp_footer(); ?>
</body>
</html>
